package com.itsjbe;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value = "com.itsjbe.*")
public class ItsjBeApplicationTests {
}
