package com.itsjbe.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsjbe.entity.User;
import com.itsjbe.presentation.AuthenMapping;
import com.itsjbe.presentation.BuyProduct;
import com.itsjbe.presentation.request.BuyProductRequest;
import com.itsjbe.presentation.request.ProductSearchRequset;
import com.itsjbe.presentation.response.BaseResponse;
import com.itsjbe.presentation.response.BillResponse;
import com.itsjbe.provider.JwtProvider;
import com.itsjbe.repository.UserRepository;
import com.itsjbe.service.ProductService;
import java.security.Principal;
import java.sql.Date;
import java.util.Arrays;
import java.util.Optional;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.web.multipart.MultipartFile;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private ProductService productService;

  @MockBean
  private UserRepository repository;

  @MockBean
  private JwtProvider jwtProvider;

  private static ObjectMapper mapper;
  private static ProductSearchRequset productSearchRequset;
  private static MockMultipartFile image;
  private static String productRequest;
  private static User userResult;
  private static BuyProductRequest buyProductRequest;

  @BeforeAll
  public static void setUp() {
    mapper = new ObjectMapper();
    productSearchRequset = new ProductSearchRequset(0, 5, "name", "desc");
    image = new MockMultipartFile("file", "image.png", "image/png",
            "Some bytes".getBytes());
    productRequest ="{\"name\": \"Áo mưa\",\"price\": \"129000\",\"totals\": \"9911\"}";
    userResult = new User(1, "test@gmail.com",
            "$2a$10$ImSmhix42CnBNhVTI66iP.LNnnMT5rtLKhAZeavMGozWFyp97mHcq",
            "test", "test", new Date(System.currentTimeMillis()), "ADMIN");
    buyProductRequest = new BuyProductRequest(Arrays.asList(new BuyProduct(1, 100)));
  }

  @BeforeEach
  public void setUpEach() {
    when(jwtProvider.getSubject(anyString()))
            .thenReturn("text@gmail.com");
    when(jwtProvider.validateToken(anyString(), any(UserDetails.class)))
            .thenReturn(true);
    when(repository.findByEmail(anyString()))
            .thenReturn(Optional.of(userResult));
  }

  @Test
  public void searchProduct_WhenPassValidation() throws Exception {
    this.mockMvc
            .perform(post("/product/search")
                    .header(AuthenMapping.AUTHORIZATION_HEADER, "Bearer eyJhb...")
                    .param("key_work", "test")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsBytes(productSearchRequset)))
            .andExpect(status().isOk());
  }

  @Test
  public void createProduct_WhenPassValidation() throws Exception {
    when(productService.create(any(Optional.class), any(MultipartFile.class)))
            .thenReturn(new BaseResponse());

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
            .multipart("/product/create")
            .file(image)
            .param("product", productRequest)
            .header(AuthenMapping.AUTHORIZATION_HEADER, "Bearer eyJhb...")
            .with(request -> {
              request.setMethod("POST");
              return request;
            });

    this.mockMvc
            .perform(builder)
            .andExpect(status().isCreated());
  }

  @Test
  public void updateProduct_WhenPassValidation() throws Exception {
    when(productService.update(any(Optional.class), any(Optional.class), any(MultipartFile.class)))
            .thenReturn(new BaseResponse());

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
            .multipart("/product/update")
            .file(image)
            .param("id", "1")
            .param("product", productRequest)
            .header(AuthenMapping.AUTHORIZATION_HEADER, "Bearer eyJhb...")
            .with(request -> {
              request.setMethod("PUT");
              return request;
            });

    this.mockMvc
            .perform(builder)
            .andExpect(status().isOk());
  }

  @Test
  public void deleteProduct_WhenPassValidation() throws Exception {
    when(productService.delete(any(Optional.class)))
            .thenReturn(new BaseResponse());

    this.mockMvc
            .perform(delete("/product/delete")
                    .header(AuthenMapping.AUTHORIZATION_HEADER, "Bearer eyJhb...")
                    .param("id", "123"))
            .andExpect(status().isOk());
  }

  @Test
  public void buyProduct_WhenPassValidation() throws Exception {
    when(productService.buy(any(BuyProductRequest.class), any(Principal.class)))
            .thenReturn(new BillResponse());

    this.mockMvc
            .perform(post("/product/buy")
                    .header(AuthenMapping.AUTHORIZATION_HEADER, "Bearer eyJhb...")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsBytes(buyProductRequest)))
            .andExpect(status().isOk());
  }

  @Test
  public void getImageProduct_WhenPassValidation() throws Exception {
    when(productService.getImage(any(Optional.class)))
            .thenReturn(new byte[0]);

    this.mockMvc
            .perform(get("/product/get-image")
                    .header(AuthenMapping.AUTHORIZATION_HEADER, "Bearer eyJhb...")
                    .param("name", "image.png"))
            .andExpect(status().isOk());
  }
}
