package com.itsjbe.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsjbe.entity.User;
import com.itsjbe.presentation.AuthenMapping;
import com.itsjbe.presentation.enums.BaseResponseType;
import com.itsjbe.presentation.request.AuthenticationResquest;
import com.itsjbe.presentation.request.EditUserRequest;
import com.itsjbe.presentation.response.BaseResponse;
import com.itsjbe.presentation.response.TokenResponse;
import com.itsjbe.provider.JwtProvider;
import com.itsjbe.repository.UserRepository;
import com.itsjbe.service.impl.UserServiceImpl;
import java.security.Principal;
import java.sql.Date;
import java.util.Optional;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

  @MockBean
  private UserServiceImpl userService;

  @MockBean
  private UserRepository repository;

  @MockBean
  private JwtProvider jwtProvider;

  @Autowired
  private MockMvc mockMvc;

  private static ObjectMapper mapper;
  private static AuthenticationResquest authenticationResquest;
  private static User userRequest;
  private static User userResult;

  @BeforeAll
  public static void setUp() {
    mapper = new ObjectMapper();
    authenticationResquest = new AuthenticationResquest("test@gmail.com", "123456");
    userRequest = new User(0, "test@gmail.com","123456",
            "test", "test", new Date(1), "");
    userResult = new User(1, "test@gmail.com",
            "$2a$10$ImSmhix42CnBNhVTI66iP.LNnnMT5rtLKhAZeavMGozWFyp97mHcq",
            "test", "test", new Date(System.currentTimeMillis()), "USER");
  }

  @Test
  public void login_WhenAuthenticationNotPassValidation() throws Exception {
    this.mockMvc
            .perform(post("/user/login")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsBytes(new AuthenticationResquest())))
            .andExpect(status().isBadRequest());
  }

  @Test
  public void login_WhenAuthenticationPassValidation() throws Exception {
    when(userService.login(any(AuthenticationResquest.class)))
            .thenReturn(new TokenResponse(""));

    this.mockMvc
            .perform(post("/user/login")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsBytes(authenticationResquest)))
            .andExpect(status().isOk());
  }

  @Test
  public void register_WhenNotPassValidation() throws Exception {
    this.mockMvc
            .perform(post("/user/register")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsBytes(new User())))
            .andExpect(status().isBadRequest());
  }

  @Test
  public void register_WhenPassValidation() throws Exception {
    when(userService.register(any(User.class)))
            .thenReturn(new BaseResponse(BaseResponseType.CREATE_SUCCESS));

    this.mockMvc
            .perform(post("/user/register")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsBytes(userRequest)))
            .andExpect(status().isCreated());
  }

  @Test
  public void editInformation_WhenNotPassValidation() throws Exception {
    this.mockMvc
            .perform(put("/user/edit-information")
                    .header(AuthenMapping.AUTHORIZATION_HEADER, "")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsBytes(new EditUserRequest())))
            .andExpect(status().isForbidden());
  }

  @Test
  public void editInformation_WhenPassValidation() throws Exception {
    when(jwtProvider.getSubject(anyString()))
            .thenReturn("text@gmail.com");
    when(jwtProvider.validateToken(anyString(), any(UserDetails.class)))
            .thenReturn(true);
    when(repository.findByEmail(anyString()))
            .thenReturn(Optional.of(userResult));
    when(userService.edit(any(EditUserRequest.class), any(Principal.class)))
            .thenReturn(new BaseResponse(BaseResponseType.UPDATE_SUCCESS));

    this.mockMvc
            .perform(put("/user/edit-information")
                    .header(AuthenMapping.AUTHORIZATION_HEADER, "Bearer eyJhb...")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsBytes(userRequest)))
            .andExpect(status().isOk());
  }
}
