package com.itsjbe.service;

import com.itsjbe.presentation.request.EditUserRequest;
import com.itsjbe.repository.UserRepository;
import com.itsjbe.entity.User;
import com.itsjbe.handler.exception.BadRequestException;
import com.itsjbe.presentation.request.AuthenticationResquest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.InternalAuthenticationServiceException;


import java.security.Principal;
import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class UserServiceTest {
    @MockBean
    private UserRepository repository;
    @Autowired
    private UserService service;

    private static User resutlUser;
    private static AuthenticationResquest authenticationResquest;
    private static EditUserRequest editUserRequest;
    private static Principal principal;

    @BeforeAll
    public static void setUp() {
        resutlUser = new User(1, "test@gmail.com",
                "$2a$10$ImSmhix42CnBNhVTI66iP.LNnnMT5rtLKhAZeavMGozWFyp97mHcq",
                "test", "test", new Date(System.currentTimeMillis()), "USER");
        authenticationResquest = new AuthenticationResquest("test@gmail.com", "123456");
        editUserRequest = new EditUserRequest("123", "test1",
                "test1", new Date(System.currentTimeMillis()));
        principal = new Principal() {
            @Override
            public String getName() {
                return "test@gmail.com";
            }
        };
    }

    @Test
    public void login_WhenUserNotPassAuthentication() {
        when(repository.findByEmail(anyString()))
                .thenReturn(Optional.empty());

        assertThrows(InternalAuthenticationServiceException.class, () -> {
            service.login(authenticationResquest);
        });
    }

    @Test
    public void login_WhenUserPassAuthentication() {
        when(repository.findByEmail(anyString()))
                .thenReturn(Optional.of(resutlUser));

        assertNotNull(service.login(authenticationResquest));
    }

    @Test
    public void register_WhenEmailIsExisted() {
        when(repository.findByEmail(anyString()))
                .thenReturn(Optional.of(resutlUser));

        assertThrows(BadRequestException.class, () -> {
            service.register(resutlUser);
        });
    }

    @Test
    public void register_WhenPassValidator() {
        resutlUser.setPassword("123456");
        when(repository.findByEmail(anyString()))
                .thenReturn(Optional.empty());

        assertNotNull(service.register(resutlUser));
    }

    @Test
    public void edit_WhenUserNotPassAuthentication() {
        when(repository.findByEmail(anyString()))
                .thenReturn(Optional.empty());

        assertThrows(BadRequestException.class, () -> {
            service.edit(editUserRequest, principal);
        });
    }

    @Test
    public void edit_WhenUserPassAuthentication() {
        when(repository.findByEmail(anyString()))
                .thenReturn(Optional.of(resutlUser));

        assertNotNull(service.edit(editUserRequest, principal));
    }

}
