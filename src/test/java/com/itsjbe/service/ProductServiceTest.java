package com.itsjbe.service;


import com.itsjbe.entity.Product;
import com.itsjbe.entity.User;
import com.itsjbe.handler.exception.BadRequestException;
import com.itsjbe.presentation.BuyProduct;
import com.itsjbe.presentation.request.BuyProductRequest;
import com.itsjbe.presentation.request.ProductSearchRequset;
import com.itsjbe.repository.ProductRepository;
import com.itsjbe.repository.UserRepository;
import java.io.IOException;
import java.security.Principal;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@SpringBootTest
public class ProductServiceTest {

    @MockBean
    private ProductRepository repository;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private ProductService service;

    private static Product product;
    private static List<Product> products;
    private static ProductSearchRequset searchRequset;
    private static Optional<String> request;
    private static MultipartFile image;
    private static MultipartFile fileText;
    private static BuyProductRequest buyProductRequest;
    private static Principal principal;

    @BeforeAll
    public static void setUp() throws IOException {
        product = new Product(1, "test", 10, 10, "logo.jpg");
        products = Arrays.asList(product);
        searchRequset = new ProductSearchRequset(0, 1, "name", "desc");

        request = Optional.of("{\"name\": \"Áo mưa\",\"price\": \"129000\",\"totals\": \"933\"}");
        image = new MockMultipartFile("file", "image.png",
                "image/png", "Some bytes".getBytes());
        fileText = new MockMultipartFile("file", "text.txt",
                "text/plain", "Some bytes".getBytes());

        List<BuyProduct> buyProducts = Arrays.asList(new BuyProduct(1, 5));
        buyProductRequest = new BuyProductRequest(buyProducts);
        principal = new Principal() {
            @Override
            public String getName() {
                return "test@gmail.com";
            }
        };
    }

    @Test
    public void createProduct_WhenProductNotPassValidator() {
        assertThrows(BadRequestException.class, () -> {
            service.create(Optional.empty(), image);
        });
        assertThrows(BadRequestException.class, () -> {
            service.create(request, fileText);
        });
    }

    @Test
    public void createProduct_WhenProductPassValidator() {
        given(repository.save(any(Product.class)))
                .willReturn(product);

        assertNotNull(service.create(request, image));
    }

    @Test
    public void searchProduct_WhenProductPassValidator() {
        when(repository.findAllByName(anyString(), any(Pageable.class)))
                .thenReturn(new PageImpl<>(products));

        assertNotNull(service.search(Optional.of("test"), searchRequset));
    }

    @Test
    public void updateProduct_WhenProductNotPassValidator() {
        assertThrows(BadRequestException.class, () -> {
            service.update(Optional.empty(), request, image);
        });
        assertThrows(BadRequestException.class, () -> {
            service.update(Optional.of(1), Optional.empty(), image);
        });
        assertThrows(BadRequestException.class, () -> {
            service.update(Optional.of(1), request, fileText);
        });
    }

//    @Test
//    public void updateProduct_WhenErrorImageFile() {
//        when(repository.findById(anyInt()))
//                .thenReturn(Optional.of(product));
//        when(repository.save(any(Product.class)))
//                .thenReturn(product);
//
//        assertThrows(InternalError.class, () -> {
//            service.update(Optional.of(1), request, image);
//        });
//    }

    @Test
    public void updateProduct_WhenPassValidation() {
        when(repository.findById(anyInt()))
                .thenReturn(Optional.of(product));
        when(repository.save(any(Product.class)))
                .thenReturn(product);

        assertNotNull(service.update(Optional.of(1), request, image));
    }

    @Test
    public void deleteProduct_WhenProductNotPassValidator() {
        given(repository.findById(anyInt()))
                .willReturn(Optional.empty());

        assertThrows(BadRequestException.class, () -> {
            service.delete(Optional.empty());
        });
        assertThrows(BadRequestException.class, () -> {
            service.delete(Optional.of(1));
        });
    }

    @Test
    public void deleteProduct_WhenProductPassValidator() {
        given(repository.findById(anyInt()))
                .willReturn(Optional.of(product));

        assertNotNull(service.delete(Optional.of(1)));
    }

    @Test
    public void buyProduct_WhenProductNotPassValidator() {
        given(repository.findById(anyInt()))
                .willReturn(Optional.empty());

        assertThrows(BadRequestException.class, () -> {
            service.buy(buyProductRequest, principal);
        });
    }

    @Test
    public void buyProduct_WhenProductIsOut() {
        Product resutl = new Product(1, "test", 10, 0, "logo.jpg");
        when(repository.findById(anyInt()))
                .thenReturn(Optional.of(resutl));

        assertThrows(BadRequestException.class, () -> {
            service.buy(new BuyProductRequest(
                    Arrays.asList(new BuyProduct(1, 100))), principal);
        });
    }

    @Test
    public void buyProduct_WhenProductPassValidator() {
        User user = new User(1, "test@gmail.com",
                "$2a$10$ImSmhix42CnBNhVTI66iP.LNnnMT5rtLKhAZeavMGozWFyp97mHcq",
                "test", "test", new Date(System.currentTimeMillis()), "USER");

        when(userRepository.findByEmail(anyString()))
                .thenReturn(Optional.of(user));

        given(repository.findById(anyInt()))
                .willReturn(Optional.of(product));
        given(repository.save(any(Product.class)))
                .willReturn(product);

        assertNotNull(service.buy(buyProductRequest, principal));
    }

    @Test
    public void getImageProduct_WhenNameNotPassValidation() {
        assertThrows(BadRequestException.class, () -> {
            service.getImage(Optional.empty());
        });
        assertThrows(InternalError.class, () -> {
            service.getImage(Optional.of("test"));
        });
    }

    @Test
    public void getImageProduct_WhenNamePassValidation() {
        assertNotNull(service.getImage(Optional.of("image.png")));
    }
}
