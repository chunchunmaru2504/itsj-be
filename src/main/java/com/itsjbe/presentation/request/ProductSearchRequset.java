package com.itsjbe.presentation.request;

import com.itsjbe.shared.Message;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
public class ProductSearchRequset {
    @Min(value = 0, message = Message.SIZE_DOES_NOT_VALID)
    private int page;

    @Min(value = 1, message = Message.PAGE_DOES_NOT_VALID)
    private int size;

    private String sortField;
    private String sortType;
}
