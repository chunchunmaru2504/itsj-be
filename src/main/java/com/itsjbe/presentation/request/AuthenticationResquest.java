package com.itsjbe.presentation.request;

import com.itsjbe.shared.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationResquest {

    @NotEmpty(message = Message.IS_EMPTY)
    @Email(message = Message.INCORRECT_EMAIL_FORMAT)
    private String email;

    @NotEmpty(message = Message.IS_EMPTY)
    private String password;
}
