package com.itsjbe.presentation.request;

import com.itsjbe.shared.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EditUserRequest {
    @NotEmpty(message = Message.IS_EMPTY)
    private String password;

    @NotEmpty(message = Message.IS_EMPTY)
    private String firstName;

    @NotEmpty(message = Message.IS_EMPTY)
    private String lastName;

    @NotNull(message = Message.IS_NULL)
    private Date dateOfBirth;
}
