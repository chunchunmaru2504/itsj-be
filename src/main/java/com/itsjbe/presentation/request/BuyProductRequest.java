package com.itsjbe.presentation.request;

import com.itsjbe.presentation.BuyProduct;
import com.itsjbe.shared.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BuyProductRequest {
    @NotEmpty(message = Message.IS_EMPTY)
    private List<BuyProduct> products;
}
