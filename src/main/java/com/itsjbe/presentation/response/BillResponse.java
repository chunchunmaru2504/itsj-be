package com.itsjbe.presentation.response;

import com.itsjbe.entity.Product;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class BillResponse {
    private Long id;
    private String email;
    private String fullName;
    private Date date;
    private List<Product> products;

    public BillResponse(String email, String fullName, List<Product> products) {
        this.id = new Date().getTime();
        this.email = email;
        this.fullName = fullName;
        this.date = new Date();
        this.products = products;
    }
}
