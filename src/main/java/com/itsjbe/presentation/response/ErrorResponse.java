package com.itsjbe.presentation.response;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@NoArgsConstructor
public class ErrorResponse {
    private int status;
    private Date timestamp;
    private String message;

    public ErrorResponse(int status , String message) {
        this.status = status;
        this.timestamp = new Date();
        this.message = message;
    }
}
