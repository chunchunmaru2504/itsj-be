package com.itsjbe.presentation.response;

import com.itsjbe.presentation.enums.BaseResponseType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponse {
    private String code;
    private String message;

    public BaseResponse(BaseResponseType baseResponseType) {
        this.code = baseResponseType.code();
        this.message = baseResponseType.message();
    }

    public BaseResponse(BaseResponseType baseResponseType, String errorMessage) {
        this.code = baseResponseType.code();
        this.message = baseResponseType.message() + " " + errorMessage;
    }
}
