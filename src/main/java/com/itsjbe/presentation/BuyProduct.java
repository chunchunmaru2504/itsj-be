package com.itsjbe.presentation;

import com.itsjbe.shared.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
public class BuyProduct {

    @Min(value = 1, message = Message.ID_NOT_VALID)
    private int id;

    @Min(value = 1, message = Message.LESS_THAN_0)
    private int totals;
}
