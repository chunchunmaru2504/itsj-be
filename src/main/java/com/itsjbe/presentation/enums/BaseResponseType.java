package com.itsjbe.presentation.enums;

public enum BaseResponseType {
    CREATE_SUCCESS("1", "create-success"),
    UPDATE_SUCCESS("1", "update-success"),
    DELETE_SUCCESS("1", "delete-success"),
    FAIL("-1", "Fail");

    private String code;
    private String message;

    BaseResponseType(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String code() {
        return this.code;
    }

    public String message() {
        return this.message;
    }
}
