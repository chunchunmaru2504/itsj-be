
package com.itsjbe.presentation;

public class AuthenMapping {

    public static final String[] MATCHERS = {
            "/user/login", "/user/register",
            "/product/get-image"
    };

    public static final String[] MATCHERS_USER = {
            "/user/edit-information",
            "/product/buy", "/product/search"
    };

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String BEARER_TOKEN = "Bearer ";
}
