package com.itsjbe.shared;

public class Message {
    public static final String EMAIL_IS_EXISTED = "email-is-existed";
    public static final String EMAIL_DOES_NOT_EXIST = "email-does-not-exist";
    public static final String EMAIL_OR_PASSWORD_WRONG = "email-or-password-wrong";
    public static final String INCORRECT_EMAIL_FORMAT = "incorrect-email-format";
    public static final String IS_EMPTY = "is-empty";
    public static final String IS_NULL = "is-null";
    public static final String LESS_THAN_0 = "value-must-be-greater-than-0";
    public static final String SIZE_DOES_NOT_VALID = "size-does-not-valid";
    public static final String PAGE_DOES_NOT_VALID = "page-does-not-valid";
    public static final String ID_NOT_VALID = "id-not-valid";
    public static final String REQUEST_NOT_VALID = "request-not-valid";
    public static final String ENTITY_NOT_FOUND = "entity-not-found";
    public static final String NOT_FOUND = "not-found";
    public static final String IMAGE_NOT_VALID = "image-not-valid";
    public static final String IMAGE_NOT_FOUND = "image-not-found";
    public static final String FILE_NOT_IMAGE = "file-not-image";
    public static final String SORT_TYPE_DOES_NOT_VALID = "sort-type-does-not-valid";
    public static final String NAME_IS_EMPTY = "name-is-empty";

    //product
    public static final String PRODUCT_IS_OUT = "product-is-out";
    public static final String PRODUCT_BUY_NOT_VALID = "product-buy-not-valid";
    public static final String NOT_INSUFFICIENT = "insufficient-number-of-products";

    //user

}
