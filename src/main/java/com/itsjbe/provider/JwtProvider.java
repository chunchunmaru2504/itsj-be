package com.itsjbe.provider;

import java.util.Date;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtProvider {

	private final String SECRET = "thai";
	private final long EXPORATION = 900000000; //600s

	public String generateToken(UserDetails userDetails) {
		return "Bearer " + 
				Jwts.builder()
				.setSubject(userDetails.getUsername())
				.setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + EXPORATION))
				.signWith(SignatureAlgorithm.HS512, SECRET)
				.compact();
	}
	
    private Claims getAllClaims(String token) {
        return Jwts.parser()
        		.setSigningKey(SECRET)
        		.parseClaimsJws(token)
        		.getBody();
    }
	
    public boolean validateToken(String token, UserDetails userDetails) {
        return getSubject(token).equalsIgnoreCase(userDetails.getUsername())
        		&& !isTokenExpired(token);
    }

    public String getSubject(String token) {
        return getAllClaims(token).getSubject();
    }

    private Date getExpirationDate(String token) {
        return getAllClaims(token).getExpiration();
    }

    private boolean isTokenExpired(String token) {
        return getExpirationDate(token).before(new Date());
    }

}
