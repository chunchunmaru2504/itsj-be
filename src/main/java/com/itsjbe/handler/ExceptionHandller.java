package com.itsjbe.handler;

import com.itsjbe.ItsjBeApplication;
import com.itsjbe.handler.exception.BadRequestException;
import com.itsjbe.presentation.response.ErrorResponse;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.logging.log4j2.Log4J2LoggingSystem;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ExceptionHandller extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LogManager.getLogger(ItsjBeApplication.class);

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        FieldError error = ex.getBindingResult().getFieldError();
        String errorMessage = error.getField() + "-" + error.getDefaultMessage();

        LOGGER.warn(errorMessage);
        return ResponseEntity.badRequest().body(
                new ErrorResponse(status.value(), errorMessage));
    }

    @ExceptionHandler(BadRequestException.class)
    private ResponseEntity<Object> handleBadRequest(BadRequestException ex) {
        LOGGER.warn(ex.fillInStackTrace());
        return ResponseEntity.badRequest().body(
                new ErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage()));
    }

    @ExceptionHandler(InternalError.class)
    private ResponseEntity<Object> handleInternalServerError(InternalError ex) {
        LOGGER.warn(ex.fillInStackTrace());
        return ResponseEntity.internalServerError().body(
                new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage()));
    }
}
