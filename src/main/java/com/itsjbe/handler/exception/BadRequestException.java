package com.itsjbe.handler.exception;

import javax.persistence.PersistenceException;

public class BadRequestException extends PersistenceException {
    public BadRequestException(String message) {
        super(message);
    }
}
