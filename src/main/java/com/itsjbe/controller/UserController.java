package com.itsjbe.controller;

import com.itsjbe.presentation.request.EditUserRequest;
import com.itsjbe.entity.User;
import com.itsjbe.presentation.Mapping;
import com.itsjbe.presentation.request.AuthenticationResquest;
import com.itsjbe.service.UserService;
import java.security.Principal;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserController is the class that we will use to control the user's APIs
 *
 * @author thainq
 *
 */
@RestController
@RequestMapping(value = Mapping.USER)
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * <p>This is a method of using controls to call login service </p>
     * @param request the values use authentication user
     * @return the ResponseEntity with status 200 if method login not error
     * @since 1.0
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody @Valid AuthenticationResquest request) {
        return ResponseEntity.ok().body(userService.login(request));
    }

    /**
     * <p>This is a method of using controls to call register service </p>
     * @param request the values use creating new user
     * @return the ResponseEntity with status 201 if method register not error
     * @since 1.0
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody @Valid User request) {
        return ResponseEntity.created(null).body(userService.register(request));
    }

    /**
     * <p>This is a method of using controls to call edit information service </p>
     * @param request the values use update user
     * @param principal is the currently logged-in user
     * @return the ResponseEntity with status 200 if method edit not error
     * @since 1.0
     */
    @RequestMapping(value = "/edit-information", method = RequestMethod.PUT)
    public ResponseEntity<?> edit(@RequestBody @Valid EditUserRequest request, Principal principal) {
        return ResponseEntity.ok().body(userService.edit(request, principal));
    }

    /**
     * <p>This is a method of using controls to call get information </p>
     * @return the ResponseEntity principal
     * @since 1.1
     */
    @RequestMapping(value = "/get-information", method = RequestMethod.GET)
    public ResponseEntity<?> getInformation(Principal principal) {
        return ResponseEntity.ok().body(userService.information(principal));
    }

}
