package com.itsjbe.controller;

import com.itsjbe.presentation.Mapping;
import com.itsjbe.presentation.request.ProductSearchRequset;
import com.itsjbe.service.ProductService;
import com.itsjbe.presentation.request.BuyProductRequest;
import java.security.Principal;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * ProductController is the class that we will use to control the product's APIs.
 *
 * @author thainq
 *
 */
@RestController
@RequestMapping(value = Mapping.PRODUCT)
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * <p>This is a method of using controls to call search product service </p>
     * @param keyWork the values use search product
     * @param request the values use sort and paging
     * @return the ResponseEntity with status 200 if method search not error
     * @since 1.0
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ResponseEntity<?> search(@RequestParam("key_work") Optional<String> keyWork,
            @RequestBody @Valid ProductSearchRequset request) {
        return ResponseEntity.ok().body(productService.search(keyWork, request));
    }

    /**
     * <p>This is a method of using controls to call create product service </p>
     * @param request the values use creating new product
     * @param file the image of product
     * @return the ResponseEntity with status 201 if method create not error
     * @since 1.0
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestParam("product") Optional<String> request,
            @RequestParam(value = "file", required = false) MultipartFile file) {
        return ResponseEntity.created(null).body(productService.create(request, file));
    }

    /**
     * <p>This is a method of using controls to call create product service</p>
     * @param id the product
     * @param request the values use update product
     * @param file the image of product
     * @return the ResponseEntity with status 200 if method update not error
     * @since 1.0
     */
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<?> create(@RequestParam("id") Optional<Integer> id,
            @RequestParam("product") Optional<String> request,
            @RequestParam(value = "file", required = false) MultipartFile file) {
        return ResponseEntity.ok().body(productService.update(id, request, file));
    }

    /**
     * <p>This is a method of using controls to call delete product service </p>
     * @param id the product
     * @return the ResponseEntity with status 200 if method delete not error
     * @since 1.0
     */
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@RequestParam("id") Optional<Integer> id) {
        return ResponseEntity.ok().body(productService.delete(id));
    }

    /**
     * <p>This is a method of using controls to call buy product service </p>
     * @param request the list product buys by user
     * @param principal is the currently logged-in user
     * @return the ResponseEntity with status 200 if method buy not error
     * @since 1.0
     */
    @RequestMapping(value = "/buy", method = RequestMethod.POST)
    public ResponseEntity<?> buy(@RequestBody @Valid BuyProductRequest request, Principal principal) {
        return ResponseEntity.ok().body(productService.buy(request, principal));
    }

    /**
     * <p>This is a method of using controls to call buy product service </p>
     * @param request the list product buys by user
     * @param principal is the currently logged-in user
     * @return the ResponseEntity with status 200 if method buy not error
     * @since 1.0
     */
    @RequestMapping(value = "/get-image", method = RequestMethod.GET)
    public ResponseEntity<?> buy(@RequestParam("name") Optional<String> name) {
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(productService.getImage(name));
    }
}
