package com.itsjbe.entity;

import com.itsjbe.shared.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@Entity
@Table(name = "USERS")
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotEmpty(message = Message.IS_EMPTY)
    @Email(message = Message.INCORRECT_EMAIL_FORMAT)
    private String email;

    @NotEmpty(message = Message.IS_EMPTY)
    private String password;

    @NotEmpty(message = Message.IS_EMPTY)
    private String firstName;

    @NotEmpty(message = Message.IS_EMPTY)
    private String lastName;

    @NotNull(message = Message.IS_NULL)
    private Date dateOfBirth;

    private String role;

}
