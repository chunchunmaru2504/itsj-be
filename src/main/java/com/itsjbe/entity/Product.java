package com.itsjbe.entity;

import com.itsjbe.shared.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "PRODUCT")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotEmpty(message = Message.IS_EMPTY)
    private String name;

    @Min(value = 0, message = Message.LESS_THAN_0)
    private double price;

    @Min(value = 0, message = Message.LESS_THAN_0)
    private double totals;

    private String image;
}
