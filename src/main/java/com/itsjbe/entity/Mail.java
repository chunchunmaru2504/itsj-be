package com.itsjbe.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.util.List;

@Data
@NoArgsConstructor
public class Mail {
    private String from;
    private String to;
    private String subject;
    private String body;
    private List<String> cc;
    private List<String> bcc;
    private List<File> files;

    public Mail(String to, String subject, String body) {
        this.from = "ITSJ <thaipro1321@gmail.com>";
        this.to = to;
        this.subject = subject;
        this.body = body;
    }
}
