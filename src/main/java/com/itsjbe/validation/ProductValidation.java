package com.itsjbe.validation;

import com.itsjbe.entity.Product;
import com.itsjbe.presentation.BuyProduct;
import com.itsjbe.presentation.request.BuyProductRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

public interface ProductValidation {
    Product createValidation(Optional<String> request, MultipartFile file);
    Product updateValidation(Optional<Integer> id, Optional<String> request, MultipartFile file);
    Product deleteValidation(Optional<Integer> id);
    Boolean buyValidation(BuyProductRequest request);
    Product buyValidation(BuyProduct buyProduct);
    String getImageValidation(Optional<String> name);
}
