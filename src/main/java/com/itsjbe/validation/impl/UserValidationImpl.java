package com.itsjbe.validation.impl;

import com.itsjbe.ItsjBeApplication;
import com.itsjbe.handler.exception.BadRequestException;
import com.itsjbe.repository.UserRepository;
import com.itsjbe.entity.User;
import com.itsjbe.shared.Message;
import com.itsjbe.validation.UserValidation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Optional;

/**
 * UserValidationImpl is the class that we will use to check valid the user
 *
 * @author thainq
 *
 */
@Service
public class UserValidationImpl implements UserValidation {

    private static final Logger LOGGER = LogManager.getLogger(UserValidationImpl.class);

    @Autowired
    private UserRepository repository;

    /**
     * <p>This is a valid method of using check valid when register user</p>
     * @param user the values use register user
     * @return the true if pass valid
     * @since 1.0
     */
    @Override
    public Boolean registerValidation(User user) {
        Optional<User> result = repository.findByEmail(user.getEmail());
        if (!result.isEmpty()) {
            LOGGER.warn(Message.EMAIL_IS_EXISTED);
            throw new BadRequestException(Message.EMAIL_IS_EXISTED);
        }
        return true;
    }

    /**
     * <p>This is a valid method of using check valid when edit information</p>
     * @param principal is the currently logged-in user
     * @return the user if pass valid
     * @since 1.0
     */
    @Override
    public User editValidation(Principal principal) {
        Optional<User> result = repository.findByEmail(principal.getName());
        if (result.isEmpty()) {
            LOGGER.warn(Message.EMAIL_DOES_NOT_EXIST);
            throw new BadRequestException(Message.EMAIL_DOES_NOT_EXIST);
        }
        return result.get();
    }
}
