package com.itsjbe.validation.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsjbe.entity.Product;
import com.itsjbe.handler.exception.BadRequestException;
import com.itsjbe.repository.ProductRepository;
import com.itsjbe.presentation.BuyProduct;
import com.itsjbe.presentation.request.BuyProductRequest;
import com.itsjbe.service.impl.UserServiceImpl;
import com.itsjbe.shared.Message;
import com.itsjbe.validation.ProductValidation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

/**
 * ProductValidationImpl is the class that we will use to check valid the product
 *
 * @author thainq
 *
 */
@Service
public class ProductValidationImpl implements ProductValidation {

    private static final Logger LOGGER = LogManager.getLogger(ProductValidationImpl.class);

    @Autowired
    private ProductRepository repository;

    /**
     * <p>This is a valid method of using checks when creating new products</p>
     * @param request the values json of model product
     * @param file the image of product
     * @return the true if pass valid
     * @since 1.1
     */
    @Override
    public Product createValidation(Optional<String> request, MultipartFile file) {
        Product product;

        if (request.isEmpty()) {
            LOGGER.warn(Message.REQUEST_NOT_VALID);
            throw new BadRequestException(Message.REQUEST_NOT_VALID);
        }
        if (file != null) {
            String fileType = file.getContentType().split("/")[0];
            if (!StringUtils.endsWithIgnoreCase(fileType, "image")) {
                LOGGER.warn(Message.FILE_NOT_IMAGE);
                throw new BadRequestException(Message.FILE_NOT_IMAGE);
            }
        }

        try {
            ObjectMapper mapper = new ObjectMapper();
            product = mapper.readValue(request.get(), Product.class);
        }
        catch (Exception e) {
            LOGGER.warn("Error message", e);
            throw new InternalError(e.getMessage());
        }

        if (!StringUtils.hasText(product.getName())) {
            throw new BadRequestException(Message.NAME_IS_EMPTY);
        }
        if (product.getPrice() < 1) {
            throw new BadRequestException("price-" + Message.LESS_THAN_0);
        }
        if (product.getTotals() < 1) {
            throw new BadRequestException("totals-" + Message.NAME_IS_EMPTY);
        }
        return product;
    }

    /**
     * <p>This is a valid method of using checks when update product</p>
     * @param id the product
     * @param request the values json of model product
     * @param file the image of product
     * @return the product if pass valid
     * @since 1.0
     */
    @Override
    public Product updateValidation(Optional<Integer> id, Optional<String> request, MultipartFile file) {
        ObjectMapper mapper = new ObjectMapper();
        Product product = new Product();

        if (id.isEmpty()) {
            LOGGER.warn(Message.ID_NOT_VALID);
            throw new BadRequestException(Message.ID_NOT_VALID);
        }

        if (request.isEmpty()) {
            LOGGER.warn(Message.REQUEST_NOT_VALID);
            throw new BadRequestException(Message.REQUEST_NOT_VALID);
        }
        else {
            try {
                product = mapper.readValue(request.get(), Product.class);
            }
            catch (Exception e) {
                LOGGER.warn("Error message", e);
                throw new BadRequestException(e.getMessage());
            }

            if (!StringUtils.hasText(product.getName())) {
                LOGGER.warn(Message.NAME_IS_EMPTY);
                throw new BadRequestException(Message.NAME_IS_EMPTY);
            }
            if (product.getPrice() < 1) {
                LOGGER.warn("price-" + Message.LESS_THAN_0);
                throw new BadRequestException("price-" + Message.LESS_THAN_0);
            }
            if (product.getTotals() < 1) {
                LOGGER.warn("totals-" + Message.NAME_IS_EMPTY);
                throw new BadRequestException("totals-" + Message.NAME_IS_EMPTY);
            }
        }

        if (file != null) {
            String fileType = file.getContentType().split("/")[0];
            if (!StringUtils.endsWithIgnoreCase(fileType, "image")) {
                LOGGER.warn(Message.FILE_NOT_IMAGE);
                throw new BadRequestException(Message.FILE_NOT_IMAGE);
            }
        }

        Optional<Product> result = repository.findById(id.get());
        if (result.isEmpty()) {
            LOGGER.warn(Message.ENTITY_NOT_FOUND);
            throw new BadRequestException(Message.ENTITY_NOT_FOUND);
        }

        product.setId(result.get().getId());
        product.setImage(result.get().getImage());
        return product;
    }

    /**
     * <p>This is a valid method of using checks when delete product</p>
     * @param id the product
     * @return the product if pass valid
     * @since 1.0
     */
    @Override
    public Product deleteValidation(Optional<Integer> id) {
        if (id.isEmpty()) {
            LOGGER.warn(Message.ID_NOT_VALID);
            throw new BadRequestException(Message.ID_NOT_VALID);
        }
        Optional<Product> result = repository.findById(id.get());
        if (result.isEmpty()) {
            LOGGER.warn(Message.ENTITY_NOT_FOUND);
            throw new BadRequestException(Message.ENTITY_NOT_FOUND);
        }
        return result.get();
    }

    /**
     * <p>This is a valid method of using checks when buy product</p>
     * @param request the values json of model product
     * @return the product if pass valid
     * @since 1.0
     */
    @Override
    public Boolean buyValidation(BuyProductRequest request) {
        if (request.getProducts().isEmpty()) {
            LOGGER.warn(Message.PRODUCT_BUY_NOT_VALID);
            throw new BadRequestException(Message.PRODUCT_BUY_NOT_VALID);
        }
        return true;
    }

    /**
     * <p>This is a valid method of using checks when buy product</p>
     * @param buyProduct the product purchased
     * @return the product if pass valid
     * @since 1.0
     */
    @Override
    public Product buyValidation(BuyProduct buyProduct) {
        Optional<Product> result = repository.findById(buyProduct.getId());
        String errorMessage = "id-" + buyProduct.getId() + "-";
        if (result.isEmpty()) {
            LOGGER.warn(errorMessage + Message.NOT_FOUND);
            throw new BadRequestException(errorMessage + Message.NOT_FOUND);
        }
        if (result.get().getTotals() < 1) {
            LOGGER.warn(errorMessage + Message.PRODUCT_IS_OUT);
            throw new BadRequestException(errorMessage + Message.PRODUCT_IS_OUT);
        }
        if (result.get().getTotals() < buyProduct.getTotals()) {
            LOGGER.warn(errorMessage + Message.NOT_INSUFFICIENT);
            throw new BadRequestException(errorMessage + Message.NOT_INSUFFICIENT);
        }
        return result.get();
    }

    /**
     * <p>This is a valid method of using checks name image product</p>
     * @param name the image product
     * @return the name image if pass valid
     * @since 1.0
     */
    @Override
    public String getImageValidation(Optional<String> name) {
        if (name.isEmpty() || !StringUtils.hasText(name.get())) {
            LOGGER.warn(Message.IMAGE_NOT_VALID);
            throw new BadRequestException(Message.IMAGE_NOT_VALID);
        }
        return name.get();
    }
}
