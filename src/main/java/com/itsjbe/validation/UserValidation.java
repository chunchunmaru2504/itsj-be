package com.itsjbe.validation;

import com.itsjbe.entity.User;

import java.security.Principal;

public interface UserValidation {
    Boolean registerValidation(User user);
    User editValidation(Principal principal);
}
