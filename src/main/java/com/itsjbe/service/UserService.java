package com.itsjbe.service;

import com.itsjbe.presentation.request.EditUserRequest;
import com.itsjbe.entity.User;
import com.itsjbe.presentation.request.AuthenticationResquest;
import com.itsjbe.presentation.response.BaseResponse;
import com.itsjbe.presentation.response.TokenResponse;

import java.security.Principal;

public interface UserService {
    TokenResponse login(AuthenticationResquest request);
    BaseResponse register(User user);
    BaseResponse edit(EditUserRequest request, Principal principal);
    User information(Principal principal);
}
