package com.itsjbe.service;

import com.itsjbe.entity.Product;
import com.itsjbe.presentation.request.BuyProductRequest;
import com.itsjbe.presentation.request.ProductSearchRequset;
import com.itsjbe.presentation.response.BaseResponse;
import com.itsjbe.presentation.response.BillResponse;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.Optional;

public interface ProductService {
    Page<Product> search(Optional<String> keyWork, ProductSearchRequset request);
    BaseResponse create(Optional<String> request, MultipartFile files);
    BaseResponse update(Optional<Integer> id, Optional<String> request, MultipartFile files);
    BaseResponse delete(Optional<Integer> id);
    BillResponse buy(BuyProductRequest request, Principal principal);
    byte[] getImage(Optional<String> name);
}
