package com.itsjbe.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsjbe.entity.Mail;
import com.itsjbe.entity.Product;
import com.itsjbe.entity.User;
import com.itsjbe.handler.exception.BadRequestException;
import com.itsjbe.presentation.enums.BaseResponseType;
import com.itsjbe.presentation.request.BuyProductRequest;
import com.itsjbe.presentation.request.ProductSearchRequset;
import com.itsjbe.presentation.response.BaseResponse;
import com.itsjbe.presentation.response.BillResponse;
import com.itsjbe.repository.ProductRepository;
import com.itsjbe.repository.UserRepository;
import com.itsjbe.service.MailService;
import com.itsjbe.service.ProductService;
import com.itsjbe.shared.Message;
import com.itsjbe.shared.StogarePaths;
import com.itsjbe.validation.ProductValidation;
import freemarker.template.Configuration;
import java.io.File;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * ProductServiceImpl is the class that we will use to handler values of product as create, update, delete and search
 *
 * @author thainq
 *
 */
@Service
public class ProductServiceImpl implements ProductService {

    private static final Logger LOGGER = LogManager.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductRepository repository;

    @Autowired
    private ProductValidation validation;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MailService mailService;

    @Autowired
    private Configuration configuration;

    /**
     * <p>This is a valid method of using search product</p>
     * @param keyWork the values use search product
     * @param request the values use sort and paging
     * @return the page product when the name included in the keyword and sort if sort field not empty
     * @since 1.0
     */
    @Override
    public Page<Product> search(Optional<String> keyWork, ProductSearchRequset request) {
        int result = (int) Arrays.stream(Sort.Direction.values())
                .filter(direction -> direction.name().equalsIgnoreCase(request.getSortType()))
                .count();
        if (result == 0) {
           LOGGER.warn(Message.SORT_TYPE_DOES_NOT_VALID);
            throw new BadRequestException(Message.SORT_TYPE_DOES_NOT_VALID);
        }

        Sort sort = StringUtils.hasText(request.getSortField()) && StringUtils.hasText(request.getSortType())
                ? Sort.by(Sort.Direction.fromString(request.getSortType()), request.getSortField())
                : Sort.unsorted();
        Pageable pageable = PageRequest.of(request.getPage(), request.getSize(), sort);
        return repository.findAllByName(keyWork.orElse(null), pageable);
    }

    /**
     * <p>This is a valid method of using creating new product</p>
     * @param request the values use creating new product
     * @param file the image of product
     * @return the base response if creating success
     * @since 1.0
     */
    @Override
    public BaseResponse create(Optional<String> request, MultipartFile file) {
        Product product = validation.createValidation(request, file);
        try {
            if (file != null) {
                String fileName = file.getOriginalFilename();
                product.setImage(fileName);
                File newFile = new File(StogarePaths.STORAGE_PATH + fileName);
                Files.write(newFile.toPath(), file.getBytes(), StandardOpenOption.CREATE);
            }

            repository.save(product);
            return new BaseResponse("1", "create-success");
        } catch (Exception e) {
            LOGGER.error("Execute get ....", e);
            throw new InternalError(e.getMessage());
        }
    }

    /**
     * <p>This is a valid method of using update product</p>
     * @param id the product
     * @param request the values use update product
     * @param file the image of product
     * @return the base response if update success
     * @since 1.1
     */
    @Override
    public BaseResponse update(Optional<Integer> id, Optional<String> request, MultipartFile file) {
        Product product = validation.updateValidation(id, request, file);
        try {
            if (file != null) {
                File image = new File(StogarePaths.STORAGE_PATH + product.getImage());
                if (image.exists()) {
                    Files.delete(image.toPath());
                }
                product.setImage(file.getOriginalFilename());
                Files.write(Paths.get(StogarePaths.STORAGE_PATH + product.getImage()),
                        file.getBytes(), StandardOpenOption.CREATE);
            }

            repository.save(product);
            return new BaseResponse(BaseResponseType.UPDATE_SUCCESS);
        } catch (Exception e) {
            LOGGER.error("Execute get ....", e);
            throw new InternalError(e.getMessage());
        }
    }

    /**
     * <p>This is a valid method of using delete product</p>
     * @param id the product
     * @return the base response if delete success
     * @since 1.0
     */
    @Override
    public BaseResponse delete(Optional<Integer> id) {
        Product result = validation.deleteValidation(id);
        try {
            try {
                Files.delete(Path.of(StogarePaths.STORAGE_PATH + result.getImage()));
            } catch (Exception e) {
                LOGGER.error(e.fillInStackTrace());
            }

            repository.deleteById(id.get());
            return new BaseResponse(BaseResponseType.DELETE_SUCCESS);
        } catch (Exception e) {
            LOGGER.error("Execute get ....", e);
            throw new InternalError(e.getMessage());
        }
    }

    /**
     * <p>This is a valid method of using buy product and send mail when user buy product</p>
     * @param request the list product buy from user
     * @param principal is the currently logged-in user
     * @return the billResponse if buy success
     * @since 1.0
     */
    @Override
    public BillResponse buy(BuyProductRequest request, Principal principal) {
        List<Product> buyProducts = handlerBuyProduct(request);
        try {
            User user = userRepository.findByEmail(principal.getName()).get();
            String fullName = user.getFirstName() + " " + user.getLastName();
            BillResponse billResponse =  new BillResponse(user.getEmail(), fullName, buyProducts);
            StringWriter stringWriter = new StringWriter();

            Map<String, Object> model = new HashMap<>();
            model.put("id", billResponse.getId().toString());
            model.put("date", billResponse.getDate().toString());
            model.put("email", billResponse.getEmail());
            model.put("fullName", billResponse.getFullName());
            model.put("products", billResponse.getProducts());
            configuration.getTemplate("emailTemplate.ftlh").process(model, stringWriter);
            String html = stringWriter.getBuffer().toString();

            Mail mail = new Mail();
            mail.setTo(user.getEmail());
            mail.setSubject("HOA DON MUA HANG");
            mail.setBody(html);
            mailService.push(mail);

            return billResponse;
        } catch (Exception e) {
            LOGGER.error("Execute get ....", e);
            throw new InternalError(e.getMessage());
        }
    }

    /**
     * <p>This is a valid method of using handler list product purchased</p>
     * @param request the list product buy from user
     * @return the list product purchased include price, totals and image
     * @since 1.0
     */
    private List<Product> handlerBuyProduct(BuyProductRequest request) {
        List<Product> buyProducts = new ArrayList<>();

        validation.buyValidation(request);
        request.getProducts().forEach(buyProduct -> {
            Product product = validation.buyValidation(buyProduct);
            product.setTotals(product.getTotals() - buyProduct.getTotals());
            repository.save(product);

            product.setTotals(buyProduct.getTotals());
            product.setPrice(product.getPrice() * buyProduct.getTotals());
            buyProducts.add(product);
        });
        return buyProducts;
    }

    /**
     * <p>This is a method of using get image product</p>
     * @param name the image product
     * @return the byte image
     * @since 1.0
     */
    @Override
    public byte[] getImage(Optional<String> name) {
        String result = validation.getImageValidation(name);
        try {
           return Files.readAllBytes(Path.of(StogarePaths.STORAGE_PATH + result));
        }
        catch (Exception e) {
            LOGGER.error(Message.IMAGE_NOT_FOUND, e);
            throw new InternalError(e.getLocalizedMessage());
        }
    }
}
