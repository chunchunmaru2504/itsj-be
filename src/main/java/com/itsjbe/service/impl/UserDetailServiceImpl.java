package com.itsjbe.service.impl;

import com.itsjbe.entity.User;
import com.itsjbe.handler.exception.BadRequestException;
import com.itsjbe.repository.UserRepository;
import com.itsjbe.shared.Message;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

@Repository
public class UserDetailServiceImpl implements UserDetailsService {

	private static final Logger LOGGER = LogManager.getLogger(UserDetailServiceImpl.class);
	
	@Autowired
	private UserRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
 		Optional<User> result = repository.findByEmail(username);
		if (result.isEmpty()) {
			LOGGER.warn(Message.EMAIL_OR_PASSWORD_WRONG);
			throw new BadRequestException(Message.EMAIL_OR_PASSWORD_WRONG);
		}

		User user = result.get();
		return org.springframework.security.core.userdetails.User
				.withUsername(user.getEmail())
				.password(user.getPassword())
				.roles(user.getRole())
				.build();
	}
}
