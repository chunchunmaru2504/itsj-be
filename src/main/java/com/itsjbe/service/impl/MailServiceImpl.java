package com.itsjbe.service.impl;

import com.itsjbe.ItsjBeApplication;
import com.itsjbe.entity.Mail;
import com.itsjbe.service.MailService;
import java.nio.charset.StandardCharsets;
import javax.mail.internet.MimeMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Component
public class MailServiceImpl implements MailService {

    private static final Logger LOGGER = LogManager.getLogger(MailServiceImpl.class);

    @Autowired
    private JavaMailSender sender;

    @Override
    public void push(Mail mail) {
        try {
            MimeMessage message = sender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            helper.setFrom("ITSJ <thaipro1321@gmail.com>");
            helper.setTo(mail.getTo());
            helper.setSubject(mail.getSubject());
            helper.setText(mail.getBody(), true);
            sender.send(message);
        } catch (Exception e) {
            LOGGER.error("Execute get ....", e);
            throw new InternalError(e.getMessage());
        }
    }

}
