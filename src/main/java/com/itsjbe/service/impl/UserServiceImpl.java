package com.itsjbe.service.impl;

import com.itsjbe.presentation.request.EditUserRequest;
import com.itsjbe.provider.Sha256Provider;
import com.itsjbe.repository.UserRepository;
import com.itsjbe.presentation.enums.BaseResponseType;
import com.itsjbe.entity.User;
import com.itsjbe.presentation.enums.RolesType;
import com.itsjbe.presentation.request.AuthenticationResquest;
import com.itsjbe.presentation.response.BaseResponse;
import com.itsjbe.presentation.response.TokenResponse;
import com.itsjbe.provider.JwtProvider;
import com.itsjbe.service.UserService;
import com.itsjbe.validation.UserValidation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;

/**
 * UserServicerImpl is the class that we will use to handler values of user as login, register and edit information
 *
 * @author thainq
 *
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserValidation validation;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private Sha256Provider sha256Provider;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * <p>This is a valid method of using authentication user</p>
     * @param request the values use authentication user
     * @return the token if authentication success
     * @since 1.0
     */
    @Override
    public TokenResponse login(AuthenticationResquest request) {
        String passwordEndCoded = sha256Provider.encodeSHA256Hex(request.getPassword());
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), passwordEndCoded));
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return new TokenResponse(jwtProvider.generateToken(userDetails));
    }

    /**
     * <p>This is a valid method of using creating new user</p>
     * @param user the values use create new user
     * @return the base response if creating success
     * @since 1.0
     */
    @Override
    public BaseResponse register(User user) {
        validation.registerValidation(user);
        try {
            user.setPassword(passwordEncoder.encode(sha256Provider.encodeSHA256Hex(user.getPassword())));
            user.setRole(RolesType.USER.toString());
            repository.save(user);
            return new BaseResponse(BaseResponseType.CREATE_SUCCESS);
        } catch (Exception e) {
            LOGGER.error("Execute get ....", e);
            throw new InternalError(e.getLocalizedMessage());
        }
    }

    /**
     * <p>This is a valid method of using edit information user</p>
     * @param request the values use update user
     * @param principal is the currently logged-in user
     * @return the base response if update success
     * @since 1.0
     */
    @Override
    public BaseResponse edit(EditUserRequest request, Principal principal) {
        User user = validation.editValidation(principal);
        try {
            user.setPassword(passwordEncoder.encode(sha256Provider.encodeSHA256Hex(request.getPassword())));
            user.setFirstName(request.getFirstName());
            user.setLastName(request.getLastName());
            user.setDateOfBirth(request.getDateOfBirth());
            repository.save(user);
            return new BaseResponse(BaseResponseType.UPDATE_SUCCESS);
        } catch (Exception e) {
            LOGGER.error("Execute get ....", e);
            throw new InternalError(e.getLocalizedMessage());
        }
    }

    /**
     * <p>This is a valid method of using get information user</p>
     * @param principal is the currently logged-in user
     * @return the base response if update success
     * @since 1.0
     */
    @Override
    public User information(Principal principal) {
        return repository.findByEmail(principal.getName()).get();
    }
}
