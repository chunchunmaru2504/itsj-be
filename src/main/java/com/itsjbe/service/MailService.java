package com.itsjbe.service;

import com.itsjbe.entity.Mail;

public interface MailService {
    void push(Mail mail);
}
