package com.itsjbe.config;

import com.itsjbe.filter.JwtRequestFilter;
import com.itsjbe.presentation.AuthenMapping;
import com.itsjbe.presentation.enums.RolesType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * SecurityConfig is the class that we will use to config web security
 *
 * @author thainq
 *
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private JwtRequestFilter tokenFilter;

	/**
	 * <p>This is a valid method of using encoder password</p>
	 * @return the new method BCryptPasswordEncoder
	 * @since 1.0
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * <p>This is a valid method of using config http</p>
	 * @param http
	 * @since 1.0
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		
		http
			.authorizeHttpRequests()
			.antMatchers(AuthenMapping.MATCHERS).permitAll()

			.antMatchers(AuthenMapping.MATCHERS_USER)
				.hasAnyRole(RolesType.USER.name(), RolesType.ADMIN.name())
			.antMatchers(HttpMethod.GET)
				.hasAnyRole(RolesType.USER.name(), RolesType.ADMIN.name())

			.antMatchers(HttpMethod.POST).hasRole(RolesType.ADMIN.toString())
			.antMatchers(HttpMethod.PUT).hasRole(RolesType.ADMIN.toString())
			.antMatchers(HttpMethod.DELETE).hasRole(RolesType.ADMIN.toString())

			.anyRequest().authenticated();

		http
	        .sessionManagement()
	        .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		http.addFilterBefore(
				tokenFilter, 
				UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
	}
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Bean
	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
}
